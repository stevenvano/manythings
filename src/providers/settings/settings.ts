import{ BehaviorSubject } from 'rxjs/Rx';
import { Injectable } from '@angular/core';

/*
  Generated class for the SettingsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SettingsProvider {
  private pic: BehaviorSubject<String>;
  constructor() {
    this.pic = new BehaviorSubject('bg1');
  }

  toggelAppPic(val){
    this.pic.next(val)
  }

  getActivePic(){
    return this.pic.asObservable();
  }
}
