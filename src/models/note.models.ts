import { createChangeDetectorRef } from "@angular/core/src/view/refs";

export interface Note {
title: string
content: string
date: Date
createDate: number
}
