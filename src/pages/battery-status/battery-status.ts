import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { BatteryStatus } from '@ionic-native/battery-status';
import { Geolocation } from '@ionic-native/geolocation';
import { Brightness } from '@ionic-native/brightness';

@IonicPage()
@Component({
  selector: 'page-battery-status',
  templateUrl: 'battery-status.html',
  providers: [BatteryStatus]
})
export class BatteryStatusPage {
  battLevel: any;
  battStatus: any;
  subscription: any;
  color: any;
  latitude: any;
  longitude: any;
  constructor(public navCtrl: NavController,
              private batteryStatus: BatteryStatus,
              private geolocation: Geolocation,
              private brightness:Brightness){
      var temp = this;
      setInterval(function(){
        console.log("interval");
        temp.checkBattStatus();
        temp.getLocation();
      },1000);

  }

  checkBattStatus(){
    this.subscription = this.batteryStatus.onChange().subscribe(
      (status) => {
        console.log(status.level, status.isPlugged);
        this.battLevel = status.level;
        let brightnessValue = this.battLevel/100;
        this.brightness.setBrightness(brightnessValue);

        this.battStatus = status.isPlugged;
        if(this.battLevel>21)
        {
          this.color = ""
        }
        if(status.isPlugged == true){
          this.battStatus = "Charging";
        }
        else{
          this.battStatus = "not Charging";
        }
      }
    )

    this.subscription = this.batteryStatus.onLow().subscribe(status => {
      console.log("Low battery");
      this.color  ="Battery low";
    })

    this.subscription = this.batteryStatus.onCritical().subscribe(status => {
      console.log("Critical battery");
      this.color  ="Battery critical";
    })
  }

  getLocation() {
    this.geolocation.getCurrentPosition().then(pos => {
    this.latitude = pos.coords.latitude;
    this.longitude = pos.coords.longitude;
         console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
       });
       let watch = this.geolocation.watchPosition().subscribe(pos => {
         console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
       });
       // to stop watching
       watch.unsubscribe();
  }

  getLatitude() {
    return "Latitude: " + this.latitude;
  }

  getLongitude() {
    return "Longitude: " + this.longitude;
  }
  /*stopBattCheck(){
    this.subscription.unsubscribe();
  }*/
  swipe(event) {
    if(event.direction === 4) {
      this.navCtrl.parent.select(2);
    }
  }
}
