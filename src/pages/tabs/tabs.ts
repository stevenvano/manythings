import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { NotesPage } from './../notes/notes';
import { CameraPage } from './../camera/camera';
import { BatteryStatusPage } from '../battery-status/battery-status'

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = NotesPage;
  tab3Root = CameraPage;
  tab4Root = BatteryStatusPage;

  constructor() {

  }


}
