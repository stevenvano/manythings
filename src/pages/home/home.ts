import { SettingsProvider } from './../../providers/settings/settings';
import { NavController } from 'ionic-angular';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Vibration } from '@ionic-native/vibration';
import { Flashlight } from '@ionic-native/flashlight';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  items: any[];
  selectedPic: String;
  isFlashLightOn:boolean=false;
  constructor(public navCtrl: NavController
    ,private platform: Platform,
    private settings: SettingsProvider,
    private vibration: Vibration,
    public flashlight: Flashlight)
  {
    this.settings.getActivePic().subscribe(val => this.selectedPic = val);
  }
  toggelAppPic(){
    if(this.selectedPic == 'bg1'){
      this.settings.toggelAppPic('bg2');
    } else{
      this.settings.toggelAppPic('bg1');
    }
  }
  vibrate(){              // *       *       *       -       *       *       *       *       -       *       -       *
    this.vibration.vibrate([100,100,100,100,100,300,300,300,100,300,100,100,100,100,100,100,300,300,100,300,300,100,100]); //2 seconden
  }

  async turnFlashlightOff(){
    try {
      await this.platform.ready();
      this.isFlashLightOn = await this.flashlight.switchOff();
    } catch (error) {
      console.error(error);
    }
  }

  async turnFlashlightOn(){
    try {
      await this.platform.ready();
      this.isFlashLightOn = await this.flashlight.switchOn();
    } catch (error) {
      console.error(error);
    }
  }

  async toggleFlashlight(){
    try {
      await this.platform.ready();
      const availableOnDevice = await this.flashlight.available();
      if(availableOnDevice){
        this.flashlight.toggle();
        this.isFlashLightOn = await this.flashlight.isSwitchedOn();
      } else {
        console.log("Not available");
      }
    } catch (error) {
      console.error(error);
    }
  }

  swipe(event) {
    if(event.direction === 2) {
      this.navCtrl.parent.select(1);
    }
  }
}
