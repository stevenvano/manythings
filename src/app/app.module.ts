import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { AddNotePage } from './../pages/add-note/add-note';
import { NotesPage } from '../pages/notes/notes';
import { ViewNotePage } from './../pages/view-note/view-note';
import { BatteryStatusPage } from './../pages/battery-status/battery-status';
import { CameraPage } from './../pages/camera/camera';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NoteService } from '../providers/note-service/note-service';
import { IonicStorageModule } from '@ionic/storage';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BatteryStatus } from '@ionic-native/battery-status';
import { SettingsProvider } from '../providers/settings/settings';
import { Vibration } from '@ionic-native/vibration';
import { Flashlight } from '@ionic-native/flashlight';
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { Brightness } from '@ionic-native/brightness';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    NotesPage,
    AddNotePage,
    ViewNotePage,
    BatteryStatusPage,
    CameraPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    FormsModule,
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    NotesPage,
    AddNotePage,
    ViewNotePage,
    BatteryStatusPage,
    CameraPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BatteryStatus,
    Vibration,
    Flashlight,
    Brightness,
    Camera,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NoteService,
    SettingsProvider
  ]
})
export class AppModule {}
